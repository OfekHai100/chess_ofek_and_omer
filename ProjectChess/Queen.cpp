#include "Queen.h"

/*
	This is the c'tor of the queen, its sets its type by its inputed color, its color and its starting location
	input: bool player_relate - the color of the queen(true - white, false - black), std::string loc - the starting location of the queen
	output: none
*/
Queen::Queen(bool player_relate, std::string loc)
{
	if (player_relate)
	{
		this->_soldier_type = QueenWhite;
	}
	else
	{
		this->_soldier_type = QueenBlack;
	}
	this->_soldeir_color = player_relate;;
	this->_soldier_loc = loc;
}

/*
	This method is the virtual move of the queen - its special movement. It gets the location that the queen needs to move to and checks if it can do it. it returns defines of every kind of move type that is set in an enum of moves by the situation of the move.
	It also uses the board in order to validate the moves
	input: std::string loc - the location that the queen needs to move to, Board& board - our board
	output: the type of the move (from the enum of defines)
*/
char Queen::move(std::string loc, Board& board)//loc is for the dest location
{
	char char_to_return = ValidMove;
	//first we need to check whether the soldier is able to move the tile even (no same-colour-soldier is there)	
	bool isAbleToMoveThere = this->is_able_to_move(loc, board);//check if the soldier even have acces to this tile
	int cnt = 0; //counting of the soldiers on the queen way (for the check chess)
	if (this->_soldier_loc != loc)
	{
		if (isAbleToMoveThere)
		{
			if (board.get_soldier_from_tile(loc) != nullptr)
			{
				if (board.get_soldier_from_tile(loc)->get_soldier_color() != this->get_soldier_color())//if there is soldier on location we check that its not our soldier
				{
					board.kill_soldier_on_tile(loc);
					board.move_soldier(this->_soldier_loc, loc);
				}
				else
				{
					char_to_return = NotValidOurSoldierOnLoc;//soldier from same color is on this tile!
				}

				if (isAbleToMoveThere && (board.get_soldier_from_tile(loc)->get_soldier_type() == KingBlack || board.get_soldier_from_tile(loc)->get_soldier_type() == KingWhite) && char_to_return != NotValidOurSoldierOnLoc) //check if there is a check mat
				{
					char_to_return = ValidCheckMat;
				}
				else if (isAbleToMoveThere && char_to_return != ValidCheckMat)
				{
					for (int i = 0; i < CHESSBOARD && char_to_return != ValidChess; i++)
					{
						if (board.is_player_exist_on_tile(board.num_to_loc(board.letter_to_int(loc[0]), i)))
						{
							if (((board.get_soldier_from_tile(board.num_to_loc(board.letter_to_int(loc[0]), i))->get_soldier_type() == KingBlack && this->get_soldier_color()) || (board.get_soldier_from_tile(board.num_to_loc(board.letter_to_int(loc[0]), i))->get_soldier_type() == KingWhite && !this->get_soldier_color()) && char_to_return != NotValidOurSoldierOnLoc) && cnt == 1) //check if there is a chess
							{
								char_to_return = ValidChess;
							}
							else
							{
								cnt++;
							}
						}
					}

					cnt = 0;
					if (char_to_return != ValidChess)
					{
						for (int i = 0; i < CHESSBOARD && char_to_return != ValidChess; i++)
						{
							if (board.is_player_exist_on_tile(board.num_to_loc(i, board.num_to_int(loc[1]))))
							{
								if (((board.get_soldier_from_tile(board.num_to_loc(i, board.num_to_int(loc[1])))->get_soldier_type() == KingBlack && this->get_soldier_color()) || (board.get_soldier_from_tile(board.num_to_loc(i, board.num_to_int(loc[1])))->get_soldier_type() == KingWhite && !this->get_soldier_color()))&& cnt == 1) //check if there is a chess
								{
									char_to_return = ValidChess;
								}
								else
								{
									cnt++;
								}
							}
						}
					}
				}
			}
			else
			{
				board.move_soldier(this->_soldier_loc, loc);
				char_to_return = ValidMove;
			}
		}
		else
		{
			char_to_return = NotValidSoldierMove;
		}
	}
	else
	{
		char_to_return = NotValidSameLoc;
	}

	return char_to_return;
}

/*
	This method checks if the queen is able to move to the new location by its special moves.
	input: std::string new_loc - the location that the queen needs to move to, Board& board - our board
	output: true - the queen is able to move to that location by its special move, false - it can't move to the location
*/
bool Queen::is_able_to_move(std::string new_loc, Board& board)
{
	//up down right left
	bool is_able = false;
	if (this->_soldier_loc[0] == new_loc[0])
	{
		if (this->is_able_to_move_up(board, new_loc[1] - this->_soldier_loc[1]) && new_loc[1] - this->_soldier_loc[1] > 0)
		{
			is_able = true;
		}
		else if (this->is_able_to_move_down(board, this->_soldier_loc[1] - new_loc[1]) && this->_soldier_loc[1] - new_loc[1] > 0 && !is_able)
		{
			is_able = true;
		}
		else
		{
			is_able = false;
		}
	}
	else if (this->_soldier_loc[1] == new_loc[1] && !is_able)
	{
		if (this->is_able_to_move_left(board, this->_soldier_loc[0] - new_loc[0]) && this->_soldier_loc[0] - new_loc[0] > 0)
		{
			is_able = true;
		}
		else if (this->is_able_to_move_right(board, new_loc[0] - this->_soldier_loc[0]) && new_loc[0] - this->_soldier_loc[0] > 0 && !is_able)
		{
			is_able = true;
		}
		else
		{
			is_able = false;
		}
	}
	else if (this->_soldier_loc[0] != new_loc[0] && !is_able)
	{
		if (this->is_able_to_move_right_up(board, this->loc_to_num_of_steps(new_loc)) || this->is_able_to_move_right(board, this->loc_to_num_of_steps(new_loc)))
		{
			is_able = true;
		}
		else
		{
			is_able = false;
		}
	}
	else
	{
		is_able = false;
	}
	return is_able;
}

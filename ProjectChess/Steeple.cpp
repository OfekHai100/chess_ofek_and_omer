#include "Steeple.h"

/*
	This is the c'tor of the steeple, its sets its type by its inputed color, its color and its starting location
	input: bool player_relate - the color of the steeple(true - white, false - black), std::string loc - the starting location of the steeple
	output: none
*/
Steeple::Steeple(bool player_relate, std::string loc)
{
	if (player_relate)
	{
		this->_soldier_type = SteepleWhite;
	}
	else
	{
		this->_soldier_type = SteepleBlack;
	}
	this->_soldeir_color = player_relate;
	this->_soldier_loc = loc;
}

/*
	This method is the virtual move of the steeple - its special movement. It gets the location that the steeple needs to move to and checks if it can do it. it returns defines of every kind of move type that is set in an enum of moves by the situation of the move.
	It also uses the board in order to validate the moves
	input: std::string loc - the location that the steeple needs to move to, Board& board - our board
	output: the type of the move (from the enum of defines)
*/
char Steeple::move(std::string loc, Board& board)//loc is for the dest location
{
	char char_to_return = ValidMove;
	//first we need to check whether the soldier is able to move the tile even (no same-colour-soldier is there)	
	bool isAbleToMoveThere = true;
	if(this->_soldier_loc[0] != loc[0] && this->_soldier_loc[1] != loc[1])
	{ 
		char_to_return = NotValidSoldierMove;
	}

	if (this->_soldier_loc != loc && char_to_return == ValidMove)
	{
		isAbleToMoveThere = this->is_able_to_move(loc, board);//check if the soldier even have acces to this tile

		if (isAbleToMoveThere)//means no one is in the way
		{
			if (board.get_soldier_from_tile(loc) != nullptr)
			{
				if (board.get_soldier_from_tile(loc)->get_soldier_color() != this->get_soldier_color())//if there is soldier on location we check that its not our soldier
				{
					if ((board.get_soldier_from_tile(loc)->get_soldier_type() == KingBlack || board.get_soldier_from_tile(loc)->get_soldier_type() == KingWhite)) //check if there is a check mat
					{
						char_to_return = ValidCheckMat;
					}
					board.kill_soldier_on_tile(loc);
				}
				else
				{
					char_to_return = NotValidOurSoldierOnLoc;//soldier from same color is on this tile!
				}
			}
			else
			{
				if (board.is_chess(*this))
				{
					char_to_return = ValidChess;
				}
				else
				{
					char_to_return = ValidMove; 
				}
			}
			board.move_soldier(this->_soldier_loc, loc);
		}
		else
		{
			char_to_return = NotValidSoldierMove;
		}
	}
	else if (char_to_return != NotValidChessOnUs)
	{
		char_to_return = NotValidSameLoc;
	}
	return char_to_return;
}

/*
	This method checks if the steeple is able to move to the new location by its special moves.
	input: std::string new_loc - the location that the steeple needs to move to, Board& board - our board
	output: true - the steeple is able to move to that location by its special move, false - it can't move to the location
*/
bool Steeple::is_able_to_move(std::string new_loc, Board& board)
{
	//up down right left
	bool is_able = false;
	if (this->_soldier_loc[0] == new_loc[0])
	{
		if (this->_soldier_loc[1] > new_loc[1] && this->is_able_to_move_down(board, this->_soldier_loc[1] - new_loc[1] + 1))
		{
			is_able = true;
		}
		else if (this->is_able_to_move_up(board, new_loc[1] - this->_soldier_loc[1]))//up and down
		{
			is_able = true;
		}
		else
		{
			is_able = false;
		}
	}
	else if (this->_soldier_loc[1] == new_loc[1] && !is_able)
	{

		if (this->_soldier_loc[0] > new_loc[0] && this->is_able_to_move_down(board, this->_soldier_loc[0] - new_loc[0]))
		{
			is_able = true;
		}
		else if (this->is_able_to_move_up(board, new_loc[0] - this->_soldier_loc[0]))
		{
			is_able = true;
		}
		else
		{
			is_able = false;
		}

	}
	else
	{
		is_able = false;
	}
	return is_able;
}

/*
This file servers as an example of how to use Pipe.h file.
It is recommended to use the following code in your project, 
in order to read and write information from and to the Backend
*/

#include "Pipe.h"
#include <iostream>
#include <thread>
#include "Board.h"


using std::cout;
using std::endl;
using std::string;


int main()
{
	Board * b = new Board();
	Board temp;
	srand(time_t(NULL));
	
	Pipe p;
	bool isConnect = p.connect();
	
	string ans;
	while (!isConnect)
	{
		cout << "cant connect to graphics" << endl;
		cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
		std::cin >> ans;

		if (ans == "0")
		{
			cout << "trying connect again.." << endl;
			Sleep(5000);
			isConnect = p.connect();
		}
		else 
		{
			p.close();
			return 1;
		}
	}
	

	char msgToGraphics[1024];
	// msgToGraphics should contain the board string accord the protocol
	// YOUR CODE

	strcpy_s(msgToGraphics, "rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR1"); // just example...
	//strcpy_s(msgToGraphics, "r##k###rpp############################################PPR##K###R1");

	p.sendMessageToGraphics(msgToGraphics);   // send the board string


	// get message from graphics
	string msgFromGraphics;// = p.getMessageFromGraphics();
	std::string old_loc;
	std::string new_loc;
	char string_to_send[2] = {0};
	bool turn_of_player = false;//black is starting//might be in board but for now its here
	General_Soldier* temp_soldier;
	while (msgFromGraphics != "quit")
	{
		msgFromGraphics = p.getMessageFromGraphics();
		std::cout << "message from graphics: " << msgFromGraphics << std::endl;
		old_loc = "";
		new_loc = "";
		old_loc += msgFromGraphics[0];
		old_loc += msgFromGraphics[1];
		new_loc += msgFromGraphics[2];
		new_loc += msgFromGraphics[3];
		temp_soldier = (b)->get_soldier_from_tile(old_loc);
		
		if (temp_soldier != nullptr)
		{
			if (temp_soldier->get_soldier_color() == turn_of_player)
			{
				string_to_send[0] = (temp_soldier->move(new_loc, temp));
				string_to_send[1] = NULL;
			}
			else
			{
				string_to_send[0] = '2';
			}
		}
		else
		{
			string_to_send[0] = '2';
		}
		strcpy_s(msgToGraphics, string_to_send); // msgToGraphics should contain the result of the operation


		// return result to graphics		
		p.sendMessageToGraphics(msgToGraphics);   

		if(msgToGraphics[0] == ValidMove || msgToGraphics[0] == ValidChess || msgToGraphics[0] == ValidCheckMat)
		{ 
			b->move_soldier(old_loc, new_loc);

			if (turn_of_player)
			{
				turn_of_player = false;
			}
			else
			{
				turn_of_player = true;
			}
		}

		b->printBoard();

	}

	p.close();
	return 0;
}
#pragma once
#include "General_Soldier.h"
#include <cmath>

class Runner : public General_Soldier
{
public:
	Runner(bool player_relate, std::string loc);
	virtual char move(std::string loc, Board& board);
	virtual bool is_able_to_move(std::string new_loc, Board& board);
};



#pragma once
#include <iostream>
#include <vector>
#include "Board.h"
#include <string>
class Board;

enum soldier_type
{
	QueenWhite = 'q',
	KingWhite = 'k',
	HorsemanWhite = 'h',
	RunnerWhite = 'r',
	SteepleWhite = 's',
	PedestrianWhite = 'p',
	QueenBlack = 'Q',
	KingBlack = 'K',
	HorsemanBlack = 'H',
	RunnerBlack = 'R',
	SteepleBlack = 'S',
	PedestrianBlack = 'P'
};

enum moveType
{
	ValidMove = '0',
	ValidChess = '1',
	NotValidNoSoldier = '2',
	NotValidOurSoldierOnLoc = '3',
	NotValidChessOnUs = '4',
	NotValidIndex = '5',
	NotValidSoldierMove = '6',
	NotValidSameLoc = '7',
	ValidCheckMat = '8'
};

class General_Soldier
{
protected:
	bool _soldeir_color;
	soldier_type _soldier_type;
	std::string _soldier_loc;
	int loc_to_num_of_steps(std::string loc);//this is for cross walking

	
public:
	virtual bool is_able_to_move(std::string new_loc, Board& board) = 0;
	virtual bool is_able_to_move_right(Board& board, int num_of_steps);
	virtual bool is_able_to_move_left(Board& board, int num_of_steps);
	virtual bool is_able_to_move_up(Board& board, int num_of_steps);
	virtual bool is_able_to_move_down(Board& board, int num_of_steps);
	virtual bool is_able_to_move_right_up(Board& board, int num_of_steps);
	virtual bool is_able_to_move_right_down(Board& board, int num_of_steps);
	virtual bool is_able_to_move_left_up(Board& board, int num_of_steps);
	virtual bool is_able_to_move_left_down(Board& board, int num_of_steps);

	virtual char move(std::string loc, Board& board) = 0;	
	bool get_soldier_color();
	std::string get_soldier_loc();
	soldier_type& get_soldier_type();
	void set_soldier_type(soldier_type type);
	void set_soldier_loc(std::string new_loc);
};


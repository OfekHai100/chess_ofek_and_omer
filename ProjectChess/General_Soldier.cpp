#include "General_Soldier.h"

/*
	This method sets the location of a certain soldier by getting to its field and inputing the inputed string of the tile
	input: std::string new_loc - the location of the soldier that needs to be set to
	output: none
*/
void General_Soldier::set_soldier_loc(std::string new_loc)
{
	this->_soldier_loc = new_loc;
}

/*
	This method calculates the number of steps that the soldier walked by its old location and its new location and returns the number of its steps
	input: std::string loc - the location that the soldier needs to move to
	output: the number of the steps that the soldier walked
*/
int General_Soldier::loc_to_num_of_steps(std::string loc)
{
	int j = Board::letter_to_int(this->_soldier_loc[1]);
	bool is_found = false;
	int steps = 0;
	if (Board::letter_to_int(this->_soldier_loc[0]) > Board::letter_to_int(loc[0]) && Board::letter_to_int(this->_soldier_loc[1]) > Board::letter_to_int(loc[1]))
	{
	//mean upper right corner look alike
		for (int i = Board::letter_to_int(this->_soldier_loc[0]); i >= 1 && !is_found; i--)
		{
			if (Board::num_to_loc(i, j) == loc)
			{
				is_found = true;
			}
			j--; 
			steps++;
		}
	}
	else if (Board::letter_to_int(this->_soldier_loc[0]) < Board::letter_to_int(loc[0]) && Board::letter_to_int(this->_soldier_loc[1]) < Board::letter_to_int(loc[1]))
	{
		//mean down right corner look alike
		for (int i = Board::letter_to_int(this->_soldier_loc[0]); i < CHESSBOARD && !is_found; i++)
		{
			if (Board::num_to_loc(i, j) == loc)
			{
				is_found = true;
			}
			j++;
			steps++;
		}
	}
	else if (Board::letter_to_int(this->_soldier_loc[0]) > Board::letter_to_int(loc[0]) && Board::letter_to_int(this->_soldier_loc[1]) < Board::letter_to_int(loc[1]))
	{
		//mean down right corner look alike
		for (int i = Board::letter_to_int(this->_soldier_loc[0]); i >= 1 && !is_found; i--)
		{
			if (Board::num_to_loc(i, j) == loc)
			{
				is_found = true;
			}
			j++;
			steps++;
		}
	}
	else if (Board::letter_to_int(this->_soldier_loc[0]) < Board::letter_to_int(loc[0]) && Board::letter_to_int(this->_soldier_loc[1]) > Board::letter_to_int(loc[1]))
	{
		//mean down right corner look alike
		for (int i = Board::letter_to_int(this->_soldier_loc[0]); i >= 1 && !is_found; i++)
		{
			if (Board::num_to_loc(i, j) == loc)
			{
				is_found = true;
			}
			j--;
			steps++;
		}
	}
	if (!is_found)
	{
		steps = -1;
	}
	return steps;
}

//this is only checking if the soldier is able to move right and if the new location is there
/*
	This method confirms if the soldier can walk to the right or not and it does it by using the board and the number of the steps that the soldier needs to walk
	input: Board& board - our board, int num_of_steps - the number of steps that the soldier needs to walk in order to get to the tile
	output: true - the soldier can walk to the location on the right, false - it can't move to the location on the right
*/
bool General_Soldier::is_able_to_move_right(Board& board, int num_of_steps)
{
	bool is_able = true;//which mean havent found the wanted tile
	bool is_no_on_on_the_way = true;
	int y = board.num_to_int(this->_soldier_loc[1]);
	int cnt = 0;
	if (board.letter_to_int(this->_soldier_loc[0]) + 1 >= CHESSBOARD)
	{
		is_able = false;
	}
	for (int i = board.letter_to_int(this->_soldier_loc[0])+1; i < num_of_steps && cnt < num_of_steps && i < CHESSBOARD && is_no_on_on_the_way && is_able; i++)
	{
		if (board.get_soldier_from_tile(i, y) != nullptr)
		{
			if (cnt == num_of_steps - 1 && this->get_soldier_color() != board.get_soldier_from_tile(y, i)->get_soldier_color())
			{
				board.kill_soldier_on_tile(board.num_to_loc(y, board.num_to_int(i)));
			}
			else
			{
				is_no_on_on_the_way = false;
				is_able = false;
			}
		}
		cnt++;
	}
	return is_able;
}

//this is only checking if the soldier is able to move left and if the new location is there
/*
	This method confirms if the soldier can walk to the left or not and it does it by using the board and the number of the steps that the soldier needs to walk
	input: Board& board - our board, int num_of_steps - the number of steps that the soldier needs to walk in order to get to the tile
	output: true - the soldier can walk to the location on the left, false - it can't move to the location on the left
*/
bool General_Soldier::is_able_to_move_left(Board& board, int num_of_steps)
{
	bool is_able = true;//which mean havent found the wanted tile
	bool is_no_on_on_the_way = true;
	int y = board.num_to_int(this->_soldier_loc[1]);
	int cnt = 0;
	if (board.letter_to_int(this->_soldier_loc[0]) - 1 < 1)
	{
		is_able = false;
	}
	for (int i = board.letter_to_int(this->_soldier_loc[0]) - 1; i >= 1 && cnt < num_of_steps && i < CHESSBOARD && is_no_on_on_the_way && is_able; i--)
	{
		if (board.get_soldier_from_tile(i, y) != nullptr)
		{
			if (cnt == num_of_steps - 1 && this->get_soldier_color() != board.get_soldier_from_tile(y, i)->get_soldier_color())
			{
				board.kill_soldier_on_tile(board.num_to_loc(y, board.num_to_int(i)));
			}
			else
			{
				is_no_on_on_the_way = false;
				is_able = false;
			}
		}
		cnt++;
	}
	return is_able;
}

//this is only checking if the soldier is able to move down and if the new location is there
/*
	This method confirms if the soldier can walk down or not and it does it by using the board and the number of the steps that the soldier needs to walk
	input: Board& board - our board, int num_of_steps - the number of steps that the soldier needs to walk in order to get to the tile
	output: true - the soldier can walk to the location down, false - it can't move to the location down
*/
bool General_Soldier::is_able_to_move_down(Board& board, int num_of_steps)
{
	bool is_able = true;//which mean havent found the wanted tile
	bool is_no_on_on_the_way = true;
	int x = board.letter_to_int(this->_soldier_loc[0]);
	int cnt = 0;
	if (board.num_to_int(this->_soldier_loc[1]) - 1 < 1)
	{
		is_able = false;
	}
	for (int i = board.num_to_int(this->_soldier_loc[1]) - 1; i >= 1 && cnt <num_of_steps &&i < CHESSBOARD && is_no_on_on_the_way && is_able; i--)
	{
		if (board.get_soldier_from_tile(x, i) != nullptr)
		{
			if (cnt == num_of_steps - 1 && this->get_soldier_color() != board.get_soldier_from_tile(x, i)->get_soldier_color())
			{
				board.kill_soldier_on_tile(board.num_to_loc(x, board.num_to_int(i)));
			}
			else
			{
				is_no_on_on_the_way = false;
				is_able = false;
			}
		}
		cnt++;
	}
	return is_able;
}

//this is only checking if the soldier is able to move up and if the new location is there
/*
	This method confirms if the soldier can walk up or not and it does it by using the board and the number of the steps that the soldier needs to walk
	input: Board& board - our board, int num_of_steps - the number of steps that the soldier needs to walk in order to get to the tile
	output: true - the soldier can walk to the location up, false - it can't move to the location up
*/
bool General_Soldier::is_able_to_move_up(Board& board, int num_of_steps)
{
	bool is_able = true;//which mean havent found the wanted tile
	bool is_no_on_on_the_way = true;
	int x = board.letter_to_int(this->_soldier_loc[0]);
	int cnt = 0;
	if (board.num_to_int(this->_soldier_loc[1]) + 1 >= CHESSBOARD)
	{
		is_able = false;
	}
	for (int i = board.num_to_int(this->_soldier_loc[1]) + 1; i >= 1 && cnt < num_of_steps && i < CHESSBOARD && is_no_on_on_the_way && is_able; i++)
	{
		if (board.get_soldier_from_tile(x, i) != nullptr)
		{
			if (cnt == num_of_steps-1 && this->get_soldier_color() != board.get_soldier_from_tile(x,i)->get_soldier_color())
			{
				board.kill_soldier_on_tile(board.num_to_loc(x, board.num_to_int(i)));
			}
			else
			{
				is_no_on_on_the_way = false;
				is_able = false;
			}
		}
		cnt++;
	}
	return is_able;
}

//this is only checking if the soldier is able to move right down and if the new location is there
/*
	This method confirms if the soldier can walk right down or not and it does it by using the board and the number of the steps that the soldier needs to walk
	input: Board& board - our board, int num_of_steps - the number of steps that the soldier needs to walk in order to get to the tile
	output: true - the soldier can walk to the location right down, false - it can't move to the location right down
*/
bool General_Soldier::is_able_to_move_right_down(Board& board, int num_of_steps)
{
	bool is_able = true;//which mean havent found the wanted tile
	bool is_no_on_on_the_way = true;
	int y = board.num_to_int(this->_soldier_loc[1])-1;
	int cnt = 0;
	if (board.letter_to_int(this->_soldier_loc[0]) + 1 >= CHESSBOARD || y < 1)
	{
		is_able = false;
	}
	for (int i = board.letter_to_int(this->_soldier_loc[0]) + 1; i < num_of_steps && cnt < num_of_steps && y < CHESSBOARD &&i < CHESSBOARD && is_no_on_on_the_way && is_able; i++)
	{
		if (board.get_soldier_from_tile(i, y) != nullptr)
		{
			is_no_on_on_the_way = false;
			is_able = true;
		}
		y--;
		cnt++;
	}
	return is_able;
}

//this is only checking if the soldier is able to move up right and if the new location is there
/*
	This method confirms if the soldier can walk right up or not and it does it by using the board and the number of the steps that the soldier needs to walk
	input: Board& board - our board, int num_of_steps - the number of steps that the soldier needs to walk in order to get to the tile
	output: true - the soldier can walk to the location right up, false - it can't move to the location right up
*/
bool General_Soldier::is_able_to_move_right_up(Board& board, int num_of_steps)
{
	bool is_able = true;//which mean havent found the wanted tile
	bool is_no_on_on_the_way = true;
	int y = board.num_to_int(this->_soldier_loc[1])+1;
	int cnt = 0;
	if (board.letter_to_int(this->_soldier_loc[0]) + 1 >= CHESSBOARD || y >= CHESSBOARD)
	{
		is_able = false;
	}
	for (int i = board.letter_to_int(this->_soldier_loc[0]) + 1; i < num_of_steps && cnt < num_of_steps && y >= 1 && i < CHESSBOARD && is_no_on_on_the_way && is_able; i++)
	{
		if (board.get_soldier_from_tile(i, y) != nullptr)
		{
			is_no_on_on_the_way = false;
			is_able = false;
		}
		y++;
		cnt++;
	}
	return is_able;
}

//this is only checking if the soldier is able to move left down and if the new location is there
/*
	This method confirms if the soldier can walk left down or not and it does it by using the board and the number of the steps that the soldier needs to walk
	input: Board& board - our board, int num_of_steps - the number of steps that the soldier needs to walk in order to get to the tile
	output: true - the soldier can walk to the location left down, false - it can't move to the location left down
*/
bool General_Soldier::is_able_to_move_left_down(Board& board, int num_of_steps)
{
	bool is_able = true;//which mean havent found the wanted tile
	bool is_no_on_on_the_way = true;
	int y = board.num_to_int(this->_soldier_loc[1]) - 1 ;
	int cnt = 0;
	if (y < 1 || board.letter_to_int(this->_soldier_loc[0]) - 1 < 1)
	{
		is_able = false;
	}
	for (int i = board.letter_to_int(this->_soldier_loc[0]) - 1; i >= 1 && cnt < num_of_steps && y >= 1&& y < CHESSBOARD && i < CHESSBOARD && is_no_on_on_the_way && is_able; i--)
	{
		if (board.get_soldier_from_tile(i, y) != nullptr)
		{
			is_no_on_on_the_way = false;
			is_able = false;
		}
		y--;
		cnt++;
	}
	return is_able;
}

//this is only checking if the soldier is able to move up left and if the new location is there
/*
	This method confirms if the soldier can walk left up or not and it does it by using the board and the number of the steps that the soldier needs to walk
	input: Board& board - our board, int num_of_steps - the number of steps that the soldier needs to walk in order to get to the tile
	output: true - the soldier can walk to the location left up, false - it can't move to the location left up
*/
bool General_Soldier::is_able_to_move_left_up(Board& board, int num_of_steps)
{
	bool is_able = true;//which mean havent found the wanted tile
	bool is_no_on_on_the_way = true;
	int y = board.num_to_int(this->_soldier_loc[1])+1;
	int cnt = 0;
	if (board.letter_to_int(this->_soldier_loc[0]) - 1 < 1 || board.num_to_int(this->_soldier_loc[1]) + 1 >= CHESSBOARD)
	{
		is_able = false;
	}
	for (int i = board.letter_to_int(this->_soldier_loc[0]) - 1; i >= 1 && cnt < num_of_steps && y >= 1 && i < CHESSBOARD && is_no_on_on_the_way && is_able; i--)
	{
		if (board.get_soldier_from_tile(i, y) != nullptr)
		{
			is_no_on_on_the_way = false;
			is_able = false;
		}
		y++;
		cnt++;
	}
	return is_able;
}

/*
	This method returns the color of the soldier by using its private field
	input: none
	output: the color of the player (true - white, false - black)
*/
bool General_Soldier::get_soldier_color()
{
	return this->_soldeir_color;
}

/*
	This method returns the location of the soldier (its tile) in string and it gets it by using the soldier's private fields
	input: none
	output: the location of the soldier in string
*/
std::string General_Soldier::get_soldier_loc()
{
	return this->_soldier_loc;
}

/*
	This method returns the type of the soldier (its define) and it gets it by using the soldier's private fields
	input: none
	output: the type of the soldier by the enum
*/
soldier_type& General_Soldier::get_soldier_type()
{
	return this->_soldier_type;
}

/*
	This method sets the type of the soldier in its field of type and by the inputed type
	input: soldier_type type - the type of the soldier(its define)
	output: none
*/
void General_Soldier::set_soldier_type(soldier_type type)
{
	this->_soldier_type = type;
}

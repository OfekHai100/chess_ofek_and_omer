#include "King.h"

/*
	This is the c'tor of the king, its sets its type by its inputed color, its color and its starting location
	input: bool player_relate - the color of the king(true - white, false - black), std::string loc - the starting location of the king
	output: none
*/
King::King(bool player_relate, std::string loc)
{
	if (player_relate)
	{
		this->_soldier_type = soldier_type::KingWhite;
	}
	else
	{
		this->_soldier_type = soldier_type::KingBlack;
	}
	this->_soldeir_color = player_relate;;
	this->_soldier_loc = loc;
	
}

/*
	This method is the virtual move of the king - its special movement. It gets the location that the king needs to move to and checks if it can do it. it returns defines of every kind of move type that is set in an enum of moves by the situation of the move.
	It also uses the board in order to validate the moves
	input: std::string loc - the location that the king needs to move to, Board& board - our board
	output: the type of the move (from the enum of defines)
*/
char King::move(std::string loc, Board& board)//loc is for the dest location
{
	char char_to_return = ValidMove;
	//first we need to check whether the soldier is able to move the tile even (no same-colour-soldier is there)	
	bool isAbleToMoveThere = this->is_able_to_move(loc, board);

	if (this->_soldier_loc != loc)
	{
		if (isAbleToMoveThere)
		{
			if (board.get_soldier_from_tile(loc) != nullptr)
			{
				if (board.get_soldier_from_tile(loc)->get_soldier_color() != this->get_soldier_color())
				{
					board.kill_soldier_on_tile(loc);
					board.move_soldier(this->_soldier_loc, loc);
				}
				else
				{
					char_to_return = NotValidOurSoldierOnLoc;//soldier from same color is on this tile!
				}

				if ((board.get_soldier_from_tile(loc)->get_soldier_type() == KingBlack || board.get_soldier_from_tile(loc)->get_soldier_type() == KingBlack) && char_to_return != NotValidOurSoldierOnLoc) //check if there is a check mat
				{
					char_to_return = ValidCheckMat;
					board.move_soldier(this->_soldier_loc, loc);
				}
				else if (board.is_chess(*this))
				{
					char_to_return = ValidChess;
					board.move_soldier(this->_soldier_loc, loc);
				}
				else
				{
					char_to_return = ValidMove;
					board.move_soldier(this->_soldier_loc, loc);
				}

			}
			

		}
		else
		{
			char_to_return = NotValidSoldierMove;
		}
	}
	else
	{
		char_to_return = NotValidSameLoc;
	}
	return char_to_return;
}

/*
	This method checks if the king is able to move to the new location by its special moves.
	input: std::string new_loc - the location that the king needs to move to, Board& board - our board
	output: true - the king is able to move to that location by its special move, false - it can't move to the location
*/
bool King::is_able_to_move(std::string new_loc, Board& board)
{
	std::string old_loc = this->get_soldier_loc();
	int old_loc_x = board.letter_to_int(old_loc[0]);//this is x of old point
	int old_loc_y = board.num_to_int(old_loc[1]);//this is y of old point
	int new_loc_x = board.letter_to_int(new_loc[0]);//this is x of new point
	int new_loc_y = board.num_to_int(new_loc[1]);//this is y of new point
	if (this->is_self_chess(new_loc, board))
	{
		if (
			(old_loc_x == new_loc_x && old_loc_y + 1 == new_loc_y && old_loc_y + 1 < 8) ||//up
			(old_loc_x == new_loc_x && old_loc_y - 1 == new_loc_y && old_loc_y - 1 >= 1) ||//down
			(old_loc_y == new_loc_y && old_loc_x + 1 == new_loc_x && old_loc_x + 1 < 8) ||//right
			(old_loc_y == new_loc_y && old_loc_x - 1 == new_loc_x && old_loc_x - 1 >= 1) ||//left
			(old_loc_y + 1 == new_loc_y && old_loc_x + 1 == new_loc_x && old_loc_x + 1 < 8 && old_loc_y + 1 < 8) ||//right up
			(old_loc_x + 1 == new_loc_x && old_loc_y - 1 == new_loc_y && old_loc_y - 1 >= 1 && old_loc_x + 1 < 8) ||//right down
			(old_loc_y + 1 == new_loc_y && old_loc_x - 1 == new_loc_x && old_loc_x - 1 >= 1 && old_loc_y + 1 < 8) ||//left up
			(old_loc_y - 1 == new_loc_y && old_loc_x - 1 == new_loc_x && old_loc_x - 1 >= 1 && old_loc_y - 1 >= 1) //left down
			)
		{
			return true;
		}
	}
	return false;
}

/*
	This method checks if the own king is in danger or not if it moves to the new location. 
	Means, it checks all of the possible moves that the other soldiers of the other player can do
	input: std::string new_loc - the location that the king needs to move to, Board& board - our board
	output: true - the king is in danger if it moves to the location, false - the king is not in danger if it moves to the location
*/
bool King::is_self_chess(std::string new_loc, Board& board)
{
	//we check all the soldiers ability to move to the given location
	General_Soldier* temp_soldier;
	bool self_chess = false;
	int old_loc_x = board.letter_to_int(this->get_soldier_loc()[0]);//this is x of old point
	int old_loc_y = board.num_to_int(this->get_soldier_loc()[1]);//this is y of old point
	int new_loc_x = board.letter_to_int(new_loc[0]);//this is x of new point
	int new_loc_y = board.num_to_int(new_loc[1]);//this is y of new point
	for (int x = 0; x < CHESSBOARD && !self_chess; x++)
	{
		for (int y = 0; y < CHESSBOARD && !self_chess; y++)
		{
			temp_soldier = board.get_soldier_from_tile(board.num_to_loc(x, y));
			if (temp_soldier != nullptr)
			{
				if (temp_soldier->get_soldier_color() != this->get_soldier_color())//same group cant kill//
				{
					if(temp_soldier->get_soldier_type() != KingBlack && temp_soldier->get_soldier_type() != KingWhite)
					{
						if (temp_soldier->move(new_loc, board) == ValidMove || temp_soldier->move(new_loc, board) == ValidChess || temp_soldier->move(new_loc, board) == ValidCheckMat)
						{
							self_chess = true;
						}
					}
					else
					{
						if (
							(old_loc_x == new_loc_x && old_loc_y + 1 == new_loc_y && old_loc_y + 1 < 8) ||//up
							(old_loc_x == new_loc_x && old_loc_y - 1 == new_loc_y && old_loc_y - 1 >= 1) ||//down
							(old_loc_y == new_loc_y && old_loc_x + 1 == new_loc_x && old_loc_x + 1 < 8) ||//right
							(old_loc_y == new_loc_y && old_loc_x - 1 == new_loc_x && old_loc_x - 1 >= 1) ||//left
							(old_loc_y + 1 == new_loc_y && old_loc_x + 1 == new_loc_x && old_loc_x + 1 < 8 && old_loc_y + 1 < 8) ||//right up
							(old_loc_x + 1 == new_loc_x && old_loc_y - 1 == new_loc_y && old_loc_y - 1 >= 1 && old_loc_x + 1 < 8) ||//right down
							(old_loc_y + 1 == new_loc_y && old_loc_x - 1 == new_loc_x && old_loc_x - 1 >= 1 && old_loc_y + 1 < 8) ||//left up
							(old_loc_y - 1 == new_loc_y && old_loc_x - 1 == new_loc_x && old_loc_x - 1 >= 1 && old_loc_y - 1 >= 1) //left down
							)
						{
							self_chess = true;
						}
					}
				}
			}
		}
	}
	return self_chess;
}

#pragma once
#include "General_Soldier.h"

class Horseman : public General_Soldier
{
public:
	Horseman(bool player_relate, std::string loc);
	virtual char move(std::string loc, Board& board);
	virtual bool is_able_to_move(std::string new_loc, Board& board);
};


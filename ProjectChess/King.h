#pragma once
#include "General_Soldier.h"

class King : public General_Soldier
{
public:
	King(bool player_relate, std::string loc);
	virtual char move(std::string loc, Board& board);
	virtual bool is_able_to_move(std::string new_loc, Board& board);
	bool is_self_chess(std::string new_loc, Board& board);
};
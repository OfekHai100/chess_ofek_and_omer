#pragma once
#include "General_Soldier.h"

#define NOT_EXIST "-1"

class Pedestrian : public General_Soldier
{
private:
	bool _start;
	bool _can_kill;
public:
	Pedestrian(bool player_relate, std::string loc);
	virtual char move(std::string loc, Board& board);
	virtual bool is_able_to_move(std::string new_loc, Board& board);
	std::string get_up_right();
	std::string get_up_left();
};


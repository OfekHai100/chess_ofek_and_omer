#include "Board.h"
#include "General_Soldier.h"
#include "Steeple.h"
#include "King.h"
#include "Pedestrian.h"
#include "Horseman.h"
#include "Queen.h"
#include "Runner.h"

/*
c'tor of class board
initiating the board with soldiers
input string that represents the soldiers layout like the message like in the source.cpp
output non
*/
Board::Board()
{
	this->init_board(DEFAULT_BOARD);
}

/*
	This method prints the whole board
	input: none
	output: none
*/
void Board::printBoard()
{
	std::cout << "  a b c d e f g h " << std::endl;
	for (int i = 0; i < CHESSBOARD; i++)
	{
		std::cout << i+1 << ' ';
		for (int j = 0; j < CHESSBOARD; j++)
		{
			if (this->_board[j][i] != nullptr)
			{
				std::cout << (char)this->_board[j][i]->get_soldier_type() << ' ';				
			}
			else
			{
				std::cout << "# ";
			}
		}
		std::cout << std::endl;
	}
}

/*
	This method checks if the king of the other player is in danger and it returns a bool value that tells if it is or not.
	input: General_Soldier& soldier - the soldier that needs to be checked if it can chess the king
	output: true - the other player's king is in danger, false - the other player's king is not in danger
*/
bool Board::is_chess(General_Soldier& soldier)
{
	bool is_true = false;
	if (soldier.get_soldier_color())//if true mean this is white soldier
	{
		if (soldier.is_able_to_move(this->blackKingLoc, *this))
		{
			is_true = true;
		}
	}
	else//black soldier
	{
		if (soldier.is_able_to_move(this->whiteKingLoc, *this))
		{
			is_true = true;
		}
	}
	return is_true;
}

/*
this function is accesing the board vector of soldiers and initiatiog it by the string inputed
input: std::string new_board_array - the whole board in a string
output: none
*/
void Board::init_board(std::string new_board_array)
{
	int index = 0;
	for (int i = 0; i < CHESSBOARD; i++)
	{
		for (int j = 0; j < CHESSBOARD; j++)
		{
			switch (new_board_array[index])
			{
			case 'r':
				this->_board[j][i] = new Steeple(true, this->num_to_loc(j, i));
				break;
			case 'R':
				this->_board[j][i] = new Steeple(false, this->num_to_loc(j, i));
				break;
			case 'k':
				this->whiteKingLoc = this->num_to_loc(j, i);
				this->_board[j][i] = new King(true, this->num_to_loc(j, i));
				break;
			case 'K':
				this->blackKingLoc = this->num_to_loc(j, i);
				this->_board[j][i] = new King(false, this->num_to_loc(j, i));
				break;
			case 'p':
				this->_board[j][i] = new Pedestrian(true, this->num_to_loc(j, i));
				break;
			case 'P':
				this->_board[j][i] = new Pedestrian(false, this->num_to_loc(j, i));
				break;
			case 'n':
				this->_board[j][i] = new Horseman(true, this->num_to_loc(j, i));
				break;
			case 'N':
				this->_board[j][i] = new Horseman(false, this->num_to_loc(j, i));
				break;
			case 'q':
				this->_board[j][i] = new Queen(true, this->num_to_loc(j, i));
				break;
			case 'Q':
				this->_board[j][i] = new Queen(false, this->num_to_loc(j, i));
				break;
			case 'b':
				this->_board[j][i] = new Runner(true, this->num_to_loc(j, i));
				break;
			case 'B':
				this->_board[j][i] = new Runner(false, this->num_to_loc(j, i));
				break;
			default:
				this->_board[j][i] = nullptr;
			}
			index++;
		}
	}
}

/*
this function is clearing the board vector
input non
output non
*/
void Board::clear()
{
	delete[] this->_board;
}

/*
	This method returns the object of the soldier from the board by a string of the tile
	input: std::string tile - the tile that needs to get the soldier from
	output: the soldier itself
*/
General_Soldier* Board::get_soldier_from_tile(std::string tile)
{
	return  this->_board[this->letter_to_int(tile[0])][this->num_to_int(tile[1])];
}

/*
	This method returns the object of the soldier from the board by the x and y of the tile
	input: int x - the x of the tile that needs to get the soldier from, int y - the y of the tile that needs to get the soldier from
	output: the soldier itself
*/
General_Soldier* Board::get_soldier_from_tile(int x, int y)
{
	return this->_board[x][y];
}

/*
	This method transfers the x and y of the tile to the string of it by two chars
	input: int x - the x of the tile, int y - the y of the tile
	output: the tile in string
*/
std::string Board::num_to_loc(int x, int y)
{
	char letter = 'a'+ x;
	char num = '1'+ y ;
	std::string str(2, letter);
	str[1] = num;
	return str;
}

/*
	This method "kills" the soldier of the tile on the board by setting the tile to null and it is used for the soldier classes
	input: std::string tile - the tile of the soldier that needs to be killed
	output: none
*/
void Board::kill_soldier_on_tile(std::string tile)
{

	if (this->get_soldier_from_tile(tile) != nullptr)
	{
		delete this->get_soldier_from_tile(tile);
		this->_board[this->letter_to_int(tile[0])][this->num_to_int(tile[1])] = nullptr;
	}
}

/*
	This method converts the first char of the tile to a int number
	input: char loc - the first letter of the tile string
	output: the letter in int
*/
int Board::letter_to_int(char loc)
{	
	return (loc - 'a');
}

/*
	This method converts a number in char to int (a number from the index of the tile)
	input: char loc - the number in char
	output: the same number in int
*/
int Board::num_to_int(char loc)
{
	return (loc - '1');
}

/*
	This method checks if a soldier exists on a certain tile by checking if the tile on the board is not set to null and it returns if a soldier exists there or not
	input: std::string tile - the tile that needs to be checked for a soldier
	output: true - a soldier exists on the tile, false - a soldier does not exist on the tile
*/
bool Board::is_player_exist_on_tile(std::string tile)
{

	if (this->_board[this->letter_to_int(tile[0])][this->num_to_int(tile[1])] != nullptr)
	{
		return true;
	}
	else
	{
		return false;
	}
}

/*
	this method moves a soldier from its old tile to its new tile on the board array and it sets the old tile to null, it also sets the new tile on the soldier object itself
	if the soldier is a king, it sets its special location on the object
	input: std::string old_loc - the old tile of the soldier, std::string new_loc - the tile that the soldier needs to move to
	output: none
*/
void Board::move_soldier(std::string old_loc, std::string new_loc)
{
	this->_board[letter_to_int(new_loc[0])][num_to_int(new_loc[1])] = this->_board[letter_to_int(old_loc[0])][num_to_int(old_loc[1])];
	this->_board[letter_to_int(old_loc[0])][num_to_int(old_loc[1])] = nullptr;
	this->get_soldier_from_tile(new_loc)->set_soldier_loc(new_loc);
	if (this->_board[letter_to_int(new_loc[0])][num_to_int(new_loc[1])]->get_soldier_type() == KingWhite)
	{
		this->whiteKingLoc = (this->_board[letter_to_int(new_loc[0])][num_to_int(new_loc[1])])->get_soldier_loc();
	}
	else if (this->_board[letter_to_int(new_loc[0])][num_to_int(new_loc[1])]->get_soldier_type() == KingBlack)
	{
		this->blackKingLoc = (this->_board[letter_to_int(new_loc[0])][num_to_int(new_loc[1])])->get_soldier_loc();
	}
}


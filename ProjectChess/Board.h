#pragma once
#include <iostream>
#include <vector>
#include "General_Soldier.h"

#define CHESSBOARD 8
#define DEFAULT_BOARD "rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR"

class General_Soldier;


class Board
{
	std::string whiteKingLoc;
	std::string blackKingLoc;

private:
	General_Soldier* _board[CHESSBOARD][CHESSBOARD];
	void init_board(std::string new_board_array);
	
	
public:
	Board();
	void clear();
	static int letter_to_int(char loc);
	static int num_to_int(char loc);
	static std::string num_to_loc(int x, int y);
	bool is_player_exist_on_tile(std::string tile);
	void move_soldier(std::string old_loc, std::string new_loc);
	General_Soldier* get_soldier_from_tile(std::string tile);
	General_Soldier* get_soldier_from_tile(int x, int y);
	void kill_soldier_on_tile(std::string tile);
	void printBoard();
	bool is_chess(General_Soldier& soldier);
};


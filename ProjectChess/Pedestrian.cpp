#include "Pedestrian.h"

/*
	This is the c'tor of the pedestrian, its sets its type by its inputed color, its color and its starting location.
	It also sets special field like _start to true at the start of the game when it wasn't moving yet and _can_kill field to false and is set to true only when it can kill other soldier by his moving
	input: bool player_relate - the color of the pedestrian(true - white, false - black), std::string loc - the starting location of the pedestrian
	output: none
*/
Pedestrian::Pedestrian(bool player_relate, std::string loc)
{
	if (player_relate)
	{
		this->_soldier_type = soldier_type::PedestrianWhite;
	}
	else
	{
		this->_soldier_type = soldier_type::PedestrianBlack;
	}
	this->_soldeir_color = player_relate;;
	this->_soldier_loc = loc;
	this->_start = true;
	this->_can_kill = false;
}

/*
	This method is the virtual move of the pedestrian - its special movement. It gets the location that the pedestrian needs to move to and checks if it can do it. it returns defines of every kind of move type that is set in an enum of moves by the situation of the move.
	It also uses the board in order to validate the moves
	input: std::string loc - the location that the pedestrian needs to move to, Board& board - our board
	output: the type of the move (from the enum of defines)
*/
char Pedestrian::move(std::string loc, Board& board)//loc is for the dest location
{
	char char_to_return = ValidMove;
	bool able_to_move = this->is_able_to_move(loc, board);

	std::string up_left = this->get_up_left();
	std::string up_right = this->get_up_right();

	if (this->get_soldier_loc() == loc)
	{
		return NotValidSameLoc;
	}
	else if (!able_to_move)
	{
		return NotValidSoldierMove;
	}
	else if (board.is_player_exist_on_tile(loc) && this->_can_kill)
	{
		if (board.get_soldier_from_tile(loc)->get_soldier_color() == this->get_soldier_color())
		{
			return NotValidOurSoldierOnLoc;
		}
		else if ((((board.get_soldier_from_tile(loc)->get_soldier_type() == KingBlack) && (this->get_soldier_color())) || ((board.get_soldier_from_tile(loc)->get_soldier_type() == KingWhite) && (!this->get_soldier_color()))) && this->_can_kill)
		{
			board.kill_soldier_on_tile(loc);
			return ValidCheckMat;
		}
		else if (this->_can_kill)
		{
			board.kill_soldier_on_tile(loc);
		}
	}

	
	if (!this->_start && up_right != NOT_EXIST && board.is_player_exist_on_tile(up_right))
	{
		if (((board.get_soldier_from_tile(up_right)->get_soldier_type() == KingBlack) && this->get_soldier_color()) || ((board.get_soldier_from_tile(up_right)->get_soldier_type() == KingWhite) && !this->get_soldier_color()))
		{
			char_to_return = ValidChess;
		}
	}
	else if (!this->_start && up_left != NOT_EXIST && board.is_player_exist_on_tile(up_left))
	{
		if (((board.get_soldier_from_tile(up_left)->get_soldier_type() == KingBlack) && this->get_soldier_color()) || ((board.get_soldier_from_tile(up_left)->get_soldier_type() == KingWhite) && !this->get_soldier_color()))
		{
			char_to_return = ValidChess;
		}
	}

	if (char_to_return == ValidMove || char_to_return == ValidChess)
	{
		this->_start = false;
		board.move_soldier(this->get_soldier_loc(), loc);
	}

	return char_to_return;
}

/*
	This method checks if the pedestrian is able to move to the new location by its special moves.
	input: std::string new_loc - the location that the pedestrian needs to move to, Board& board - our board
	output: true - the pedestrian is able to move to that location by its special move, false - it can't move to the location
*/
bool Pedestrian::is_able_to_move(std::string new_loc, Board& board)
{
	std::string up_left = this->get_up_left();
	std::string up_right = this->get_up_right();

	//the soldier on the upper right or left can be killed
	if (this->get_soldier_color()) 
	{
		if ((new_loc == up_left || new_loc == up_right) && board.is_player_exist_on_tile(new_loc))
		{
			if (board.get_soldier_from_tile(new_loc)->get_soldier_color() != this->get_soldier_color())
			{
				this->_can_kill = true;
			}

			else
			{
				this->_can_kill = false;
			}
		}

		else
		{
			this->_can_kill = false;
		}
	}

	else if (!this->get_soldier_color())
	{
		if ((new_loc == up_left || new_loc == up_right) && board.is_player_exist_on_tile(new_loc))
		{
			if (board.get_soldier_from_tile(new_loc)->get_soldier_color() != this->get_soldier_color())
			{
				this->_can_kill = true;
			}

			else
			{
				this->_can_kill = false;
			}
		}

		else
		{
			this->_can_kill = false;
		}
	}
	


	if (board.letter_to_int(this->_soldier_loc[0]) != board.letter_to_int(new_loc[0]) && !this->_can_kill)
	{
		return false;
	}

	else
	{
		if (this->get_soldier_color() && !this->_start)
		{
			if (board.num_to_int(new_loc[1]) - board.num_to_int(this->_soldier_loc[1]) != 1)
			{
				return false;
			}
		}
		else if (!this->get_soldier_color() && !this->_start)
		{
			if (board.num_to_int(new_loc[1]) - board.num_to_int(this->_soldier_loc[1]) != -1)
			{
				return false;
			}
		}
		else if (this->get_soldier_color() && this->_start && (board.num_to_int(new_loc[1]) - board.num_to_int(this->_soldier_loc[1]) > 2))
		{
			return false;
		}
		else if (!this->get_soldier_color() && this->_start && (board.num_to_int(this->_soldier_loc[1]) - board.num_to_int(new_loc[1]) > 2))
		{
			return false;
		}
	}

	return true;
}

/*
	This method returns a string of a tile of the upper right location of the pedestrian and if it does not exist, because the pedestrian is on the edge, it returns a defined error
	input: none
	output: the upper right string of the location
*/
std::string Pedestrian::get_up_right()
{
	std::string up_right = "";

	if (this->get_soldier_color())
	{
		up_right += (this->get_soldier_loc()[0] + 1);
		up_right += (this->get_soldier_loc()[1] + 1);
	}

	else
	{
		up_right += (this->get_soldier_loc()[0] - 1);
		up_right += (this->get_soldier_loc()[1] - 1);
	}

	if (up_right[0] > 'h' || up_right[0] < 'a' || up_right[1] > '8' || up_right[1] < '1') //on the edge of the board
	{
		up_right = NOT_EXIST;
	}

	return up_right;
}

/*
	This method returns a string of a tile of the upper left location of the pedestrian and if it does not exist, because the pedestrian is on the edge, it returns a defined error
	input: none
	output: the upper left string of the location
*/
std::string Pedestrian::get_up_left()
{
	std::string up_left = "";

	if (this->get_soldier_color())
	{
		up_left += (this->get_soldier_loc()[0] - 1);
		up_left += (this->get_soldier_loc()[1] + 1);
	}

	else
	{
		up_left += (this->get_soldier_loc()[0] + 1);
		up_left += (this->get_soldier_loc()[1] - 1);
	}

	if (up_left[0] > 'h' || up_left[0] < 'a' || up_left[1] > '8' || up_left[1] < '1') //on the edge of the board
	{
		up_left = NOT_EXIST;
	}

	return up_left;
}
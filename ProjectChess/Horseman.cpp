#include "Horseman.h"

/*
	This is the c'tor of the horseman, its sets its type by its inputed color, its color and its starting location
	input: bool player_relate - the color of the horseman(true - white, false - black), std::string loc - the starting location of the horseman
	output: none
*/
Horseman::Horseman(bool player_relate, std::string loc)
{
	if (player_relate)
	{
		this->_soldier_type = soldier_type::HorsemanWhite;
	}
	else
	{
		this->_soldier_type = soldier_type::HorsemanBlack;
	}
	this->_soldeir_color = player_relate;;
	this->_soldier_loc = loc;
}

/*
	This method is the virtual move of the horseman - its special movement. It gets the location that the horseman needs to move to and checks if it can do it. it returns defines of every kind of move type that is set in an enum of moves by the situation of the move.
	It also uses the board in order to validate the moves
	input: std::string loc - the location that the horseman needs to move to, Board& board - our board
	output: the type of the move (from the enum of defines)
*/
char Horseman::move(std::string loc, Board& board)
{
	char char_to_return = ValidMove;
	int new_loc_x = board.letter_to_int(loc[0]);
	int new_loc_y = board.num_to_int(loc[1]);

	bool is_able = this->is_able_to_move(loc, board);
	if (this->get_soldier_loc() != loc)
	{
		if (is_able)
		{
			if (board.is_player_exist_on_tile(loc))
			{
				if (board.get_soldier_from_tile(loc)->get_soldier_color() == this->get_soldier_color())
				{
					return NotValidOurSoldierOnLoc;
				}
				else if (board.get_soldier_from_tile(loc)->get_soldier_type() == KingBlack || board.get_soldier_from_tile(loc)->get_soldier_type() == KingWhite)
				{
					board.kill_soldier_on_tile(loc);
					char_to_return = ValidCheckMat;
				}
				else
				{
					board.kill_soldier_on_tile(loc);
				}
			}
		}
		else
		{
			return NotValidSoldierMove;
		}
	}
	else
	{
		return NotValidSameLoc;
	}
	
	if (char_to_return == ValidMove)
	{
		board.move_soldier(this->get_soldier_loc(), loc);
		if (board.is_chess(*this))
		{
			char_to_return = ValidChess;
		}
	}
	return char_to_return;
}

/*
	This method checks if the horseman is able to move to the new location by its special moves.
	input: std::string new_loc - the location that the horseman needs to move to, Board& board - our board
	output: true - the horseman is able to move to that location by its special move, false - it can't move to the location
*/
bool Horseman::is_able_to_move(std::string new_loc, Board& board)
{
	int old_loc_x = board.letter_to_int(this->get_soldier_loc()[0]);
	int old_loc_y = board.num_to_int(this->get_soldier_loc()[1]);
	int new_loc_x = board.letter_to_int(new_loc[0]);
	int new_loc_y = board.num_to_int(new_loc[1]);

	int dx[] = { -2, -1, 1, 2, -2, -1, 1, 2 }; //array of possible moves in x
	int dy[] = { -1, -2, -2, -1, 1, 2, 2, 1 }; //array of possible moves in y

	for (int i = 0; i < 8; i++) //every combination of the horseman moves by the two arrays
	{
		if (new_loc_x == old_loc_x + dx[i] && new_loc_y == old_loc_y + dy[i])
		{
			return true;
		}
	}
	return false;
}
#include "Runner.h"

/*
	This is the c'tor of the runner, its sets its type by its inputed color, its color and its starting location
	input: bool player_relate - the color of the runner(true - white, false - black), std::string loc - the starting location of the runner
	output: none
*/
Runner::Runner(bool player_relate, std::string loc)
{
	if (player_relate)
	{
		this->_soldier_type = RunnerWhite;
	}
	else
	{
		this->_soldier_type = RunnerBlack;
	}
	this->_soldeir_color = player_relate;;
	this->_soldier_loc = loc;
}

/*
	This method is the virtual move of the runner - its special movement. It gets the location that the runner needs to move to and checks if it can do it. it returns defines of every kind of move type that is set in an enum of moves by the situation of the move.
	It also uses the board in order to validate the moves
	input: std::string loc - the location that the runner needs to move to, Board& board - our board
	output: the type of the move (from the enum of defines)
*/
char Runner::move(std::string loc, Board& board)
{
	char char_to_return = ValidMove;

	if (loc == this->get_soldier_loc())
	{
		return NotValidSameLoc;
	}
	
	if (this->is_able_to_move(loc, board))
	{
		if (board.is_player_exist_on_tile(loc))
		{
			if (board.get_soldier_from_tile(loc)->get_soldier_color() == this->get_soldier_color())
			{
				char_to_return = NotValidOurSoldierOnLoc;
			}
			else 
			{
				if (board.get_soldier_from_tile(loc)->get_soldier_type() == KingBlack || board.get_soldier_from_tile(loc)->get_soldier_type() == KingWhite)
				{
					char_to_return = ValidCheckMat;
				}
				else
				{
					board.kill_soldier_on_tile(loc);
				}
			}
		}
	}
	else
	{
		return NotValidSoldierMove;
	}

	if (char_to_return == ValidMove)
	{
		board.move_soldier(this->get_soldier_loc(), loc);
		if (board.is_chess(*this))
		{
			char_to_return = ValidChess;
		}
	}

	return char_to_return;
}

/*
	This method checks if the runner is able to move to the new location by its special moves.
	input: std::string new_loc - the location that the runner needs to move to, Board& board - our board
	output: true - the runner is able to move to that location by its special move, false - it can't move to the location
*/
bool Runner::is_able_to_move(std::string new_loc, Board& board)
{
	int num_of_steps = this->loc_to_num_of_steps(new_loc);
	bool is_able = false;

	if (std::abs(new_loc[0] - this->get_soldier_loc()[0]) == std::abs(new_loc[1] - this->get_soldier_loc()[1]))
	{
		if ((new_loc[0] > this->get_soldier_loc()[0]))
		{
			if (this->is_able_to_move_right_up(board, num_of_steps))
			{
				is_able = true;
			}
			else if (this->is_able_to_move_right_down(board, num_of_steps))
			{
				is_able == true;
			}
			else
			{
				is_able = false;
			}
		}
		else
		{
			if (this->is_able_to_move_left_up(board, num_of_steps))
			{
				is_able = true;
			}
			else if (this->is_able_to_move_left_down(board, num_of_steps))
			{
				is_able == true;
			}
			else
			{
				is_able = false;
			}
		}
	}
	else
	{
		return false;
	}

	return is_able;
}